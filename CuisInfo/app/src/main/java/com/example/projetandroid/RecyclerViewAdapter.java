package com.example.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projetandroid.BD.Categorie;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    List<Categorie> listeCategorie;
    Context context;

    public RecyclerViewAdapter( Context context) {
        listeCategorie = new ArrayList<>();
        this.context = context;
    }


    public void setCategorie(List<Categorie> categories){
        this.listeCategorie = categories;
       notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.une_ligne_categorie,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_region_titre.setText(listeCategorie.get(position).getNom_cat());
        Glide.with(this.context).load(listeCategorie.get(position).getImageUrl()).into(holder.iv_region_image);


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,activity_liste_recette.class);
                intent.putExtra("nom_cat",listeCategorie.get(position).getNom_cat());
                context.startActivity(intent);
            }
        });

   //     holder.btn_recette_edit.setOnClickListener(new View.OnClickListener() {
   //         @Override
   //         public void onClick(View v) {
   //             Intent intent = new Intent(context,editRecette.class);
   //             context.startActivity(intent);
   //         }
   //     });
    }

    @Override
    public int getItemCount() {
        return this.listeCategorie.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_region_image;
        TextView tv_region_titre;
        ConstraintLayout parentLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_region_image = itemView.findViewById(R.id.iv_image_cat);
            tv_region_titre = itemView.findViewById(R.id.tv_cat_titre);
            parentLayout = itemView.findViewById(R.id.uneLigneCatLayout);
        }
    }
}


