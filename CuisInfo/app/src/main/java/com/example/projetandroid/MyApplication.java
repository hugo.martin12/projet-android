package com.example.projetandroid;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.projetandroid.BD.Categorie;
import com.example.projetandroid.BD.CuisinfoViewModel;
import com.example.projetandroid.BD.Recette;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyApplication extends Application {

    private static CuisinfoViewModel cuisinfoViewModel;

    public MyApplication() {
        //cuisinfoViewModel = new ViewModelProvider(this.getApplicationContext()).get(CuisinfoViewModel.class);
    }


    public static LiveData<List<Categorie>> getListeCategorie() {
        return cuisinfoViewModel.getAllCategorie();
    }

    public static LiveData<List<Recette>> getListeRecette(){
        return cuisinfoViewModel.getAllRecette();
    }

}
