package com.example.projetandroid.BD;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CuisinfoDao {

    @Insert(onConflict = REPLACE)
    void insert_Cat(Categorie cat);

    @Insert(onConflict = REPLACE)
    void insert_Recette(Recette r);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update_Recette(Recette r);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update_Cat(Categorie cat);

    @Delete
    void delete_cat(Categorie cat);

    @Delete
    void delete_Recette(Recette r);

    @Query("DELETE From categorie")
    void deleteAllCat();

    @Query("DELETE From recette")
    void deleteAllRecette();

    @Query("SELECT * FROM categorie")
    LiveData<List<Categorie>> getALlCat();

    @Query("SELECT * FROM recette")
    LiveData<List<Recette>> getALlRecette();

    @Query("SELECT * from recette where num_recette == :id LIMIT 1")
    Recette getRecette(Integer id);

    @Query("SELECT * from categorie where num_cat == :id LIMIT 1")
    Categorie getCategorie(Integer id);

    @Query("SELECT * from recette where nom_recette == :nom LIMIT 1")
    Recette getRecetteNom(String nom);

    @Query("SELECT * from categorie where nom_cat == :nom LIMIT 1")
    Categorie getCategorieNom(String nom);

    @Transaction
    @Query("SELECT * FROM categorie")
    List<CategorieWithRecette> getCategorieWithRecette();

    @Transaction
    @Query("SELECT * FROM categorie WHERE num_cat == :id LIMIT 1")
    List<CategorieWithRecette> getCategorieWithRecetteID(Integer id);

    @Transaction
    @Query("SELECT * FROM Recette")
    List<RecetteWithCategorie> getRecetteWithCategorie();

    @Transaction
    @Query("SELECT * FROM CategorieRecetteCrossRef natural join recette WHERE num_cat == :id")
    List<RecetteWithCategorie> getRecetteWithCategorieID(Integer id);

    @Insert(onConflict = REPLACE)
    void insert(CategorieRecetteCrossRef categorieRecetteCrossRef);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(CategorieRecetteCrossRef categorieRecetteCrossRef);

    @Delete
    void delete(CategorieRecetteCrossRef categorieRecetteCrossRef);

    @Query("DELETE From Categorierecettecrossref")
    void deleteAllCatRecette();


}
