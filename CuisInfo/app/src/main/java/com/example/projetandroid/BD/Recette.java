package com.example.projetandroid.BD;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "recette")
public class Recette {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer num_recette;
    @NonNull
    private String nom_recette;
    private String image_recette;
    private Integer note_recette;
    private String ingre_recette;
    private String descrip_recette;


    public Recette(String nom_recette, String image_recette, Integer note_recette, String ingre_recette, String descrip_recette) {
        this.nom_recette = nom_recette;
        this.image_recette = image_recette;
        this.note_recette = note_recette;
        this.ingre_recette = ingre_recette;
        this.descrip_recette = descrip_recette;
    }

    public String getNom_recette() {
        return nom_recette;
    }

    public void setNom_recette(String nom_recette) {
        this.nom_recette = nom_recette;
    }

    public String getImage_recette() {
        return image_recette;
    }

    public void setImage_recette(String image_recette) {
        this.image_recette = image_recette;
    }

    public Integer getNote_recette() {
        return note_recette;
    }

    public void setNote_recette(Integer note_recette) {
        this.note_recette = note_recette;
    }

    public String getIngre_recette() {
        return ingre_recette;
    }

    public void setIngre_recette(String ingre_recette) {
        this.ingre_recette = ingre_recette;
    }

    public String getDescrip_recette() {
        return descrip_recette;
    }

    public void setDescrip_recette(String descrip_recette) {
        this.descrip_recette = descrip_recette;
    }

    public Integer getNum_recette() {
        return this.num_recette;
    }

    public void setNum_recette(Integer num_recette) {
        this.num_recette = num_recette;
    }

    @Override
    public String toString() {
        return "Recette{" +
                "num_recette=" + num_recette +
                ", nom_recette='" + nom_recette + '\'' +
                ", image_recette='" + image_recette + '\'' +
                ", note_recette=" + note_recette +
                ", ingre_recette='" + ingre_recette + '\'' +
                ", descrip_recette='" + descrip_recette + '\'' +
                '}';
    }
}
