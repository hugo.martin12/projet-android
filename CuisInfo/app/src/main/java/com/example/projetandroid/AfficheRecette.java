package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.projetandroid.BD.CuisinfoViewModel;
import com.example.projetandroid.BD.Recette;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;

public class AfficheRecette extends AppCompatActivity{

    public static final int EDIT_RECETTE = 1;

    private Recette recette;

    private String titre;
    private int idRecette;
    private String url ;
    private String ingr;
    private String desc;
    private int diff;
    private TextView tv_afficheRecette_titre;
    private TextView iv_afficheRecette_recette;
    private TextView iv_afficheRecette_ingredients;
    private ImageView iv_afficheRecette_image;
    private Button btn;
    private Button button3;
    private CuisinfoViewModel cuisinfoViewModel;

    //private YouTubePlayerView youTubePlayerView;
    //private YouTubePlayer.OnInitializedListener onInitializedListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_recette);

        Button buttonYoutube = findViewById(R.id.buttonYoutube);
        tv_afficheRecette_titre = findViewById(R.id.tv_afficheRecette_titre);
        iv_afficheRecette_recette = findViewById(R.id.iv_afficheRecette_recette);
        iv_afficheRecette_ingredients = findViewById(R.id.iv_afficheRecette_ingredients);
        iv_afficheRecette_image = findViewById(R.id.iv_afficheRecette_image);
        button3 = findViewById(R.id.button3);
        btn = findViewById(R.id.button);


        cuisinfoViewModel  = new ViewModelProvider(this).get(CuisinfoViewModel.class);

        if(savedInstanceState == null){
            Bundle bundle = getIntent().getExtras();

            this.idRecette = bundle.getInt("idRecette");

            try {
                this.recette = cuisinfoViewModel.getRecetteID(this.idRecette);
                this.setTextView();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        buttonYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AfficheRecette.this,Youtube.class);
                intent.putExtra("titreRecette",AfficheRecette.this.titre);
                startActivity(intent);
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AfficheRecette.this,Edit_recette.class);
                intent.putExtra("titre",AfficheRecette.this.titre);
                intent.putExtra("diff",AfficheRecette.this.diff);
                intent.putExtra("url",AfficheRecette.this.url);
                intent.putExtra("ingr",AfficheRecette.this.ingr);
                intent.putExtra("desc",AfficheRecette.this.desc);
                startActivityForResult(intent,EDIT_RECETTE);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuisinfoViewModel.delete_recette(AfficheRecette.this.recette);
                activity_liste_recette.setListeRecette();
                finish();
            }
        });

    }

    public void setTextView(){
        this.titre = this.recette.getNom_recette();
        this.url = this.recette.getImage_recette();
        this.ingr = this.recette.getIngre_recette();
        this.desc = this.recette.getDescrip_recette();
        this.diff = this.recette.getNote_recette();

        Glide.with(this).load(this.url).into(this.iv_afficheRecette_image);
        tv_afficheRecette_titre.setText(this.titre);
        iv_afficheRecette_recette.setText(this.desc);
        iv_afficheRecette_ingredients.setText(this.ingr);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == EDIT_RECETTE && resultCode == RESULT_OK){
            String titre = data.getStringExtra(Ajouter_recette.EXTRA_TITRE);
            int diff = data.getIntExtra(Ajouter_recette.EXTRA_DIFF,0);
            String url = data.getStringExtra(Ajouter_recette.EXTRA_URL);
            String ingr = data.getStringExtra(Ajouter_recette.EXTRA_INGR);
            String desc = data.getStringExtra(Ajouter_recette.EXTRA_DESC);


            recette.setNom_recette(titre);
            recette.setNote_recette(diff);
            recette.setImage_recette(url);
            recette.setIngre_recette(ingr);
            recette.setDescrip_recette(desc);

            cuisinfoViewModel.update_recette(recette);


            finish();
            activity_liste_recette.setListeRecette();
            this.setTextView();

        }
    }

}

