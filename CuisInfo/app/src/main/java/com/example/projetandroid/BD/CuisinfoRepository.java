package com.example.projetandroid.BD;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CuisinfoRepository {
    private CuisinfoDao cuisinfoDao;
    private LiveData<List<Recette>> allRecette;
    private LiveData<List<Categorie>> allCategorie;

    public CuisinfoRepository(Application application) throws ExecutionException, InterruptedException {
        CuisinfoRoomDatabase db = CuisinfoRoomDatabase.getDatabase(application);
        cuisinfoDao = db.cuisinfoDao();
        allCategorie = cuisinfoDao.getALlCat();
        allRecette = cuisinfoDao.getALlRecette();
    }

    public void insert_recette(Recette r){
        new InsertRecetteAsyncTask(cuisinfoDao).execute(r);
    }

    public void insert_cat(Categorie cat){new InsertCategorieAsyncTask(cuisinfoDao).execute(cat);}

    public void insert_CAT_REC(Recette r,Categorie cat){new insertCatWithRecetteAsyncTask(cuisinfoDao).execute(new AsyncTaskTwoParams(r,cat));}

    public void update_recette(Recette r){new UpdateRecetteAsyncTask(cuisinfoDao).execute(r);}

    public void update_cat(Categorie cat){new UpdateCategorieAsyncTask(cuisinfoDao).execute(cat);}

    public void update_CAT_REC(Recette r,Categorie cat){new UpdateRecetteWithCategorieAsyncTask(cuisinfoDao).execute(new AsyncTaskTwoParams(r,cat));}

    public void delete_CAT_REC(Recette r,Categorie cat){new deleteRecetteWithCategorieAsyncTask(cuisinfoDao).execute(new AsyncTaskTwoParams(r,cat));}

    public void delete_recette(Recette r){
        new DeleteRecetteAsyncTask(cuisinfoDao).execute(r);
    }

    public void delete_cat(Categorie cat){new DeleteCategorieAsyncTask(cuisinfoDao).execute(cat);}

    public void delete_all_cat(){
        new DeleteAllCategorieAsyncTask(cuisinfoDao).execute();
    }

    public void delete_all_recette(){
        new DeleteAllRecetteAsyncTask(cuisinfoDao).execute();
    }

    public void delete_all_CAT_REC(){new DeleteAll_CAT_REC_AsyncTask(cuisinfoDao).execute();}

    public LiveData<List<Recette>> getALlRecette(){
        return allRecette;
    }
    public LiveData<List<Categorie>> getALlCat(){
        return allCategorie;
    }


    public List<CategorieWithRecette> getCatWithRecette(Integer id) throws ExecutionException, InterruptedException {
        List<CategorieWithRecette> listeObjets = new getCatWithRecetteAsyncTask(cuisinfoDao).execute(id).get();
        return listeObjets;
    }

    private static class getCatWithRecetteAsyncTask extends AsyncTask<Integer, Void, List<CategorieWithRecette>>{

        private CuisinfoDao cuisinfoDao;

        private getCatWithRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected List<CategorieWithRecette> doInBackground(Integer... integers) {

            List<CategorieWithRecette> listeObjets = cuisinfoDao.getCategorieWithRecetteID(integers[0]);
            return listeObjets;
        }

    }
    public List<RecetteWithCategorie> getRecetteWithCat(Integer id) throws ExecutionException, InterruptedException {
        List<RecetteWithCategorie> listeObjets = new getRecetteWithCatAsyncTask(cuisinfoDao).execute(id).get();
        return listeObjets;
    }

    private static class getRecetteWithCatAsyncTask extends AsyncTask<Integer, Void, List<RecetteWithCategorie>>{

        private CuisinfoDao cuisinfoDao;

        private getRecetteWithCatAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected List<RecetteWithCategorie> doInBackground(Integer... integers) {

            List<RecetteWithCategorie> listeObjets = cuisinfoDao.getRecetteWithCategorieID(integers[0]);
            return listeObjets;
        }

    }


    public Recette getRecetteNom(String nom) throws ExecutionException, InterruptedException{
        Recette recette = new GetRecetteNom(cuisinfoDao).execute(nom).get();
        return recette;
    }

    public Categorie getCatNom(String nom) throws ExecutionException, InterruptedException{
        Categorie cat = new GetCatNom(cuisinfoDao).execute(nom).get();
        return cat;
    }

    public Recette getRecetteID(Integer id) throws ExecutionException, InterruptedException {
        Recette recette = new GetRecetteId(cuisinfoDao).execute(id).get();
        return recette;
    }
    public Categorie getCatID(Integer id) throws ExecutionException, InterruptedException {
        Categorie cat = new GetCatId(cuisinfoDao).execute(id).get();
        return cat;
    }


    private static class GetCatNom extends AsyncTask<String, Void, Categorie>{

        private CuisinfoDao cuisinfoDao;

        private GetCatNom(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Categorie doInBackground(String... strings) {

            Categorie cat = cuisinfoDao.getCategorieNom(strings[0]);
            return cat;
        }

    }

    private static class GetRecetteNom extends AsyncTask<String, Void, Recette>{

        private CuisinfoDao cuisinfoDao;

        private GetRecetteNom(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Recette doInBackground(String... strings) {

            Recette recette = cuisinfoDao.getRecetteNom(strings[0]);
            return recette;
        }

    }

    private static class GetRecetteId extends AsyncTask<Integer, Void, Recette>{

        private CuisinfoDao cuisinfoDao;

        private GetRecetteId(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Recette doInBackground(Integer... integers) {

            Recette recette = cuisinfoDao.getRecette(integers[0]);
            return recette;
        }

    }

    private static class GetCatId extends AsyncTask<Integer, Void, Categorie>{

        private CuisinfoDao cuisinfoDao;

        private GetCatId(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Categorie doInBackground(Integer... integers) {

            Categorie cat = cuisinfoDao.getCategorie(integers[0]);
            return cat;
        }

    }

    private static class UpdateRecetteAsyncTask extends AsyncTask<Recette,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private UpdateRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Recette... recette){
            cuisinfoDao.update_Recette(recette[0]);
            return null;
        }
    }

    private static class UpdateCategorieAsyncTask extends AsyncTask<Categorie,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private UpdateCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Categorie... cat){
            cuisinfoDao.update_Cat(cat[0]);
            return null;
        }
    }

    public static  class UpdateRecetteWithCategorieAsyncTask extends  AsyncTask<AsyncTaskTwoParams,Void,Void>{

        private  CuisinfoDao cuisinfoDao;

        public UpdateRecetteWithCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Void doInBackground(AsyncTaskTwoParams... asyncTaskTwoParams) {
            cuisinfoDao.update( new CategorieRecetteCrossRef(asyncTaskTwoParams[0].r.getNum_recette(), asyncTaskTwoParams[0].cat.getNum_cat()));
            return null;
        }
    }



    private static class InsertRecetteAsyncTask extends AsyncTask<Recette,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private InsertRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Recette... recette){
            cuisinfoDao.insert_Recette(recette[0]);
            return null;
        }
    }

    private static class InsertCategorieAsyncTask extends AsyncTask<Categorie,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private InsertCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Categorie... cat){
            cuisinfoDao.insert_Cat(cat[0]);
            return null;
        }
    }

    public static class AsyncTaskTwoParams{
        private Recette r;
        private Categorie cat;

        AsyncTaskTwoParams(Recette r, Categorie cat){
            this.r = r;
            this.cat = cat;
        }
    }

    public static  class insertCatWithRecetteAsyncTask extends  AsyncTask<AsyncTaskTwoParams,Void,Void>{

        private  CuisinfoDao cuisinfoDao;

        public insertCatWithRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Void doInBackground(AsyncTaskTwoParams... asyncTaskTwoParams) {
            cuisinfoDao.insert(new CategorieRecetteCrossRef(asyncTaskTwoParams[0].cat.getNum_cat(), asyncTaskTwoParams[0].r.getNum_recette()));
            return null;
        }
    }


    public static  class deleteRecetteWithCategorieAsyncTask extends  AsyncTask<AsyncTaskTwoParams,Void,Void>{

        private  CuisinfoDao cuisinfoDao;

        public deleteRecetteWithCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }

        @Override
        protected Void doInBackground(AsyncTaskTwoParams... asyncTaskTwoParams) {
            cuisinfoDao.delete(new CategorieRecetteCrossRef(asyncTaskTwoParams[0].r.getNum_recette(), asyncTaskTwoParams[0].cat.getNum_cat()));
            return null;
        }
    }

    private static class DeleteRecetteAsyncTask extends AsyncTask<Recette,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private DeleteRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Recette... recette){
            cuisinfoDao.delete_Recette(recette[0]);
            return null;
        }
    }

    private static class DeleteCategorieAsyncTask extends AsyncTask<Categorie,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private DeleteCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Categorie... cat){
            cuisinfoDao.delete_cat(cat[0]);
            return null;
        }
    }

    private static class DeleteAllCategorieAsyncTask extends AsyncTask<Void,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private DeleteAllCategorieAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Void... voids){
            cuisinfoDao.deleteAllCat();
            return null;
        }
    }

    private static class DeleteAllRecetteAsyncTask extends AsyncTask<Void,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private DeleteAllRecetteAsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Void... voids){
            cuisinfoDao.deleteAllRecette();
            return null;
        }
    }

    private static class DeleteAll_CAT_REC_AsyncTask extends AsyncTask<Void,Void,Void>{
        private CuisinfoDao cuisinfoDao;
        private DeleteAll_CAT_REC_AsyncTask(CuisinfoDao cuisinfoDao){
            this.cuisinfoDao = cuisinfoDao;
        }
        @Override
        protected Void doInBackground(Void... voids){
            cuisinfoDao.deleteAllCatRecette();
            return null;
        }
    }

}
