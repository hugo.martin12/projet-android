package com.example.projetandroid.BD;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutionException;

@Database(entities = {Categorie.class,Recette.class,CategorieRecetteCrossRef.class},version = 1)
public abstract class CuisinfoRoomDatabase  extends RoomDatabase {
    public abstract CuisinfoDao cuisinfoDao();

    private static CuisinfoRoomDatabase INSTANCE;

    public static CuisinfoRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (CuisinfoRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),CuisinfoRoomDatabase.class,"Cuisinfo_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(roomCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback roomCallback = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void> {
        private CuisinfoDao cuisinfoDao;

        private PopulateDbAsyncTask(CuisinfoRoomDatabase db){
            cuisinfoDao = db.cuisinfoDao();
        }
        @Override
        protected Void doInBackground(Void... voids){

            Categorie cat1 = new Categorie("Healthy","https://heyme.care/storage/2020-03-23-09-58-58-098061.jpg");
            Categorie cat2 = new Categorie("Veggie","https://4.bp.blogspot.com/-VECOsMkzd4A/VU9nBA26y8I/AAAAAAAAEwk/bZ44A7NzODM/s1600/Veggie%2Bplate%2B%231%2B1.JPG");
            Categorie cat3 = new Categorie("Fast Food","https://media-cdn.tripadvisor.com/media/photo-s/17/ad/2f/16/double-burger.jpg");
            Categorie cat4 = new Categorie("Viande","https://static.750g.com/images/622-auto/820fcd9f48110d8390cdb30a1da03939/adobestock-316536515.jpeg");
            Categorie cat5 = new Categorie("Dessert","https://res.cloudinary.com/hv9ssmzrz/image/fetch/c_fill,f_auto,h_630,q_auto,w_1200/https://s3-eu-west-1.amazonaws.com/images-ca-1-0-1-eu/tag_photos/original/404/tag-dessert-3000x2000.jpg");
            Categorie cat6 = new Categorie("Poisson","https://static.cms.yp.ca/ecms/media/2/22611063_xl-1446118863-600x360.jpg");

            Recette r1 = new Recette("Ravioles sautées",
                    "https://jow.fr/_next/image?url=https%3A%2F%2Fstatic.jow.fr%2F880x880%2Frecipes%2FQGaAtquG7g.png.webp&w=1920&q=75",
                    5,
                    "120g ravioles au fromage\2" +
                            " poignées d'épinard\10" +
                            "g de noisettes",
                    "Faites cuire les ravioles selon les instructions du paquet, égouttez-les.\n" +
                            "Faites chauffer une poêle avec un filet d'huile d'olive puis y verser les ravioles.\n" +
                            "Faites-les revenir à feu moyen pendant 2 min en les manipulant délicatement.\n" +
                            "Ajoutez les épinards frais dans la poêle et attendre quelques secondes qu'ils fondent.\n" +
                            "ervir dans des assiettes puis ajoutez les noisettes concassées, salez, poivrez. C'est prêt !");

            Recette r2 = new Recette("Veggie lasagna",
                    "https://img.cuisineaz.com/660x660/2018/03/14/i136264-lasagnes-legumes-vegan.jpeg",
                    6,
                    "50g lasagnes(cru)\n150g courgette\n150g Tommate(purée)\n80g Champignons de Paris (frai)\n1/2 Mozzarella (boule)",
                    "Préchauffez votre four à 180°C. Coupez la courgette en deux puis en tranches.\n" +
                            "Épluchez et coupez les champignons en lamelles.\n" +
                            "Dans un plat à gratin, versez un filet d'huile d'olive. Ajoutez une couche de pâtes à lasagnes, une couche de courgettes, champignons, sauce tomate et mozzarella. À nouveau une couche de pâtes et commencez un nouvel étage. Assaisonnez entre chaque couche.\n" +
                            "Une fois le dernier étage monté, terminez avec une dernière couche de pâtes et de la mozzarella. Vérifiez que les pâtes sont bien immergées dans la sauce tomate. Salez, poivrez et enfournez pour 50 minutes à 180°C.\n" +
                            "Sortez le plat du four et servez. C'est prêt !");

            Recette r3 = new Recette("Burger raclette",
                    "https://www.adeline-cuisine.fr/wp-content/uploads/2016/11/burger-savoyard-raclette-jambon-fume-maison-recette.jpg",
                    4,
                    "1 Pain burger\n" +
                            "1 Boeuf (steak haché)\n" +
                            "2 càc Confits d'oignons\n" +
                            "1 càc Moutarde à l'ancienne\n" +
                            "20g Raclette",
                    "Dans une poêle à feu moyen, ajoutez une noisette de beurre et faites toaster la face intérieure de vos pains 15-30 secondes afin qu'ils soient dorés.\n" +
                            "Dans cette même poêle, ajoutez votre steak et faites-le cuire à feu vif 2 minutes sur chaque face.\n" +
                            "Une fois le steak retourné, ajoutez le fromage sur le dessus et laissez cuire à couvert.\n" +
                            "Étalez le confit d'oignons et la moutarde sur le pain.\n" +
                            "Ajoutez le steak une fois cuit. Refermez le burger, c'est prêt, dégustez aussitôt !");
            Recette r4 = new Recette("Bœuf haricots verts & purée",
                    "https://zediet.cloudimg.io/v7/https://static.jow.fr/550x550/recipes/y6QLyk9WQL.png.webp?w=400&h=400",
                    2,
                    "100g Haricot vert (frais)\n" +
                            "250g Pomme de terre\n" +
                            "5cl Lait\n" +
                            "1 Boeuf (steak)\n" +
                            "20g Beurre",
                    "Épluchez les pommes de terre et coupez-les en dés.\n" +
                            "Faites bouillir une casserole d'eau chaude, ajoutez-y les pommes de terre et laissez cuire 15 minutes.\n" +
                            "Faites bouillir une casserole d'eau. Lavez puis équeutez les haricots si besoin. Plongez-les dans l'eau bouillante avec une pincée de sel, faire cuire 10 minutes. Égouttez.\n" +
                            "Une fois la cuisson des pommes de terre écoulée, égouttez-les puis ajoutez-les dans un récipient.\n" +
                            "Ajoutez le lait et le beurre, salez et poivrez et écrasez le tout jusqu'à former une purée de la consistance souhaitée.\n" +
                            "Faire cuire le boeuf dans une poêle à feu vif avec une noisette de beurre, 2 minutes de chaque côté pour une cuisson saignante. Salez, poivrez en fin de cuisson. Servir avec la purée et les haricots verts. C'est prêt !");

            Recette r5 = new Recette("Cake au citron",
                    "https://mapatisserie.fr/wp-content/uploads/2020/03/recette-cake-citron-20190317_143504-01-scaled.jpeg",
                    4,
                    "180g de Farine de blé\n" +
                            "1 Citron jaune\n" +
                            "100g Sucre (en poudre)\n" +
                            "3 Oeuf\n" +
                            "120g Beurre\n" +
                            "1 sac Levure chimique\n" +
                            "80g Crème fraîche épaisse",
                    "Préchauffez le four à 180°C. Dans un récipient, mélangez le beurre mou (ou passez-le au micro-ondes 15 secondes, pas plus !) et le sucre jusqu'à obtention d'un mélange crémeux.\n" +
                            "Ajoutez ensuite les oeufs, une pincée de sel, le zeste d'un citron et la crème, mélangez.\n" +
                            "Mélangez la farine et la levure puis incorporez-les progressivement dans le premier mélange.\n" +
                            "Ajoutez le jus d'1/2 citron (optionnel : ajoutez 2 cu. à soupe de graines de pavot) et mélangez afin d'obtenir une pâte lisse et crémeuse.\n" +
                            "Beurrez le moule puis versez-y la pâte. Enfournez pendant 50 minutes à 180°C. Une fois le cake cuit, laissez-le refroidir avant de déguster, c'est prêt !\n" +
                            "Bonus : réalisez une citronnade avec les restes de citron.\n" +
                            "Bonus : Si vous avez du sucre glace chez vous, réalisez un glaçage en mélangeant 100g de sucre glace et le jus d'1/2 citron jusqu'à obtention d'une texture lisse. Faites-le couler sur le dessus de cake, ajoutez quelques zestes ou graines de pavot pour la déco.");

            Recette r6 = new Recette("Saumon & haricots rôtis",
                    "https://static.jow.fr/recipes/pznPkcEFJjiZSQ.jpg",
                    2,
                    "1 càs Sauce soja salée\n" +
                            "1 càs Sirop d'érable\n" +
                            "1 Saumon (frais)\n" +
                            "250g Haricots\n" +
                            "1/4 Citron Vert\n" +
                            "1 càs Pâte miso",
                    "Préchauffez le four à 200°C. Optionnel : ajoutez de la pâte miso dans un bol.\n" +
                            "Ajoutez le sirop d'érable et la sauce soja et mélangez.\n" +
                            "Sur une plaque de cuisson recouverte de papier sulfurisé (ou dans un plat allant au four) ajoutez le pavé de saumon et les haricots.\n" +
                            "Versez la sauce sur le dessus, ajoutez un filet d'huile d'olive et enfournez pendant 10 minutes.\n" +
                            "Sortez la plaque du four, poivrez, ajoutez le jus de citron vert. (Optionnel : ajoutez de la coriandre). Servir. C'est prêt !");


            cuisinfoDao.insert_Cat(cat1);
            cuisinfoDao.insert_Cat(cat2);
            cuisinfoDao.insert_Cat(cat3);
            cuisinfoDao.insert_Cat(cat4);
            cuisinfoDao.insert_Cat(cat5);
            cuisinfoDao.insert_Cat(cat6);
            cuisinfoDao.insert_Recette(r1);
            cuisinfoDao.insert_Recette(r2);
            cuisinfoDao.insert_Recette(r3);
            cuisinfoDao.insert_Recette(r4);
            cuisinfoDao.insert_Recette(r5);
            cuisinfoDao.insert_Recette(r6);

            Categorie c1 = cuisinfoDao.getCategorieNom(cat1.getNom_cat());
            Categorie c2 = cuisinfoDao.getCategorieNom(cat2.getNom_cat());
            Categorie c3 = cuisinfoDao.getCategorieNom(cat3.getNom_cat());
            Categorie c4 = cuisinfoDao.getCategorieNom(cat4.getNom_cat());
            Categorie c5 = cuisinfoDao.getCategorieNom(cat5.getNom_cat());
            Categorie c6 = cuisinfoDao.getCategorieNom(cat6.getNom_cat());

            Recette rec1 = cuisinfoDao.getRecetteNom(r1.getNom_recette());
            Recette rec2 = cuisinfoDao.getRecetteNom(r2.getNom_recette());
            Recette rec3 = cuisinfoDao.getRecetteNom(r3.getNom_recette());
            Recette rec4 = cuisinfoDao.getRecetteNom(r4.getNom_recette());
            Recette rec5 = cuisinfoDao.getRecetteNom(r5.getNom_recette());
            Recette rec6 = cuisinfoDao.getRecetteNom(r6.getNom_recette());


            cuisinfoDao.insert(new CategorieRecetteCrossRef(c1.getNum_cat(),rec1.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c1.getNum_cat(),rec2.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c2.getNum_cat(),rec2.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c3.getNum_cat(),rec3.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c4.getNum_cat(),rec3.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c4.getNum_cat(),rec4.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c5.getNum_cat(),rec5.getNum_recette()));
            cuisinfoDao.insert(new CategorieRecetteCrossRef(c6.getNum_cat(),rec6.getNum_recette()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec2.getNum_recette()),cuisinfoViewModel.getCatID(cat1.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec2.getNum_recette()),cuisinfoViewModel.getCatID(cat2.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec3.getNum_recette()),cuisinfoViewModel.getCatID(cat3.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec3.getNum_recette()),cuisinfoViewModel.getCatID(cat4.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec4.getNum_recette()),cuisinfoViewModel.getCatID(cat4.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec5.getNum_recette()),cuisinfoViewModel.getCatID(cat5.getNum_cat()));
            //cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rec6.getNum_recette()),cuisinfoViewModel.getCatID(cat6.getNum_cat()));

            return null;

        }
    }
}
