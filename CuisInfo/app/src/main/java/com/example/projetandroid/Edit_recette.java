package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Edit_recette extends AppCompatActivity {

    public static final String EXTRA_TITRE = "package com.example.projetandroid.EXTRA_TITRE";
    public static final String EXTRA_DIFF = "package com.example.projetandroid.EXTRA_DIFF";
    public static final String EXTRA_URL = "package com.example.projetandroid.EXTRA_URL";
    public static final String EXTRA_INGR = "package com.example.projetandroid.EXTRA_INGR";
    public static final String EXTRA_DESC = "package com.example.projetandroid.EXTRA_DESC";

    private EditText et_editR_titre;
    private EditText et_editR_diff;
    private EditText et_editR_url;
    private EditText et_editR_ingr;
    private EditText et_editR_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recette);
        et_editR_titre = findViewById(R.id.et_editR_titre);
        et_editR_diff = findViewById(R.id.et_editR_diff);
        et_editR_url = findViewById(R.id.et_editR_url);
        et_editR_ingr = findViewById(R.id.et_editR_ingr);
        et_editR_desc = findViewById(R.id.et_editR_desc);

        Bundle bundle = getIntent().getExtras() ;
        et_editR_titre.setText(bundle.getString("titre"));
        et_editR_diff.setText(String.valueOf(bundle.getInt("diff")));
        et_editR_url.setText(bundle.getString("url"));
        et_editR_ingr.setText(bundle.getString("ingr"));
        et_editR_desc.setText(bundle.getString("desc"));
    }


    public void editRecette(View view) {
        String titre = et_editR_titre.getText().toString();
        int diff = Integer.parseInt(et_editR_diff.getText().toString());
        String url = et_editR_url.getText().toString();
        String ingr = et_editR_ingr.getText().toString();
        String desc = et_editR_desc.getText().toString();

        if(titre.trim().isEmpty() || et_editR_diff.getText().toString().trim().isEmpty() || url.trim().isEmpty() || ingr.trim().isEmpty() || desc.trim().isEmpty()){
            Toast.makeText(this,"Veuillez remplir les champs svp.",Toast.LENGTH_SHORT);
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_TITRE,titre);
        intent.putExtra(EXTRA_DIFF,diff);
        intent.putExtra(EXTRA_URL,url);
        intent.putExtra(EXTRA_INGR,ingr);
        intent.putExtra(EXTRA_DESC,desc);

        setResult(RESULT_OK,intent);
        finish();

    }
}