package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Ajouter_categorie extends AppCompatActivity {
    public static final String EXTRA_NOM =
            "package com.example.projetandroid.EXTRA_NOM";
    public static final String EXTRA_URL =
            "package com.example.projetandroid.EXTRA_URL";


    private EditText et_ajouterCat_url;
    private EditText et_ajouterCat_nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_categorie);

        et_ajouterCat_url = findViewById(R.id.et_ajouterCat_url);
        et_ajouterCat_nom = findViewById(R.id.et_ajouterCat_nom);
    }

    public void saveCat(View view) {
        String nom = this.et_ajouterCat_nom.getText().toString();
        String url = this.et_ajouterCat_url.getText().toString();

        if(nom.trim().isEmpty() || url.trim().isEmpty()){
            Toast.makeText(this,"Entrez un nom et une url s'il vous plait",Toast.LENGTH_SHORT);
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NOM,nom);
        data.putExtra(EXTRA_URL,url);

        setResult(RESULT_OK,data);
        finish();
    }
}