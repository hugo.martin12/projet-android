package com.example.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projetandroid.BD.Recette;
import com.example.projetandroid.BD.RecetteWithCategorie;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapterRecette  extends RecyclerView.Adapter<RecyclerViewAdapterRecette.MyViewHolder> {

    List<RecetteWithCategorie> listeRecette;
    Context context;

    public RecyclerViewAdapterRecette(Context context) {
        this.listeRecette = new ArrayList<>();
        this.context = context;
    }

    public void setListeRecette(List<RecetteWithCategorie> listeRecette){
        this.listeRecette = listeRecette;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.une_ligne_recette,parent,false);
        MyViewHolder holder = new RecyclerViewAdapterRecette.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_recette_titre.setText(listeRecette.get(position).recette.getNom_recette());
        holder.tv_recette_difficulte.setText(String.valueOf(listeRecette.get(position).recette.getNote_recette()));
        Glide.with(this.context).load(listeRecette.get(position).recette.getImage_recette()).into(holder.iv_recette_image);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AfficheRecette.class);
                intent.putExtra("idRecette",listeRecette.get(position).recette.getNum_recette());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.listeRecette.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_recette_image;
        TextView tv_recette_titre;
        TextView tv_recette_difficulte;
        ConstraintLayout parentLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_recette_image = itemView.findViewById(R.id.iv_listeR_image);
            tv_recette_titre = itemView.findViewById(R.id.tv_listeR_titre);
            tv_recette_difficulte = itemView.findViewById(R.id.tv_listeR_diff);
            parentLayout = itemView.findViewById(R.id.uneLigneRecetteLayout);
        }
    }
}
