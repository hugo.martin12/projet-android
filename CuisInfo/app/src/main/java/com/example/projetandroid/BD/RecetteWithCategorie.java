package com.example.projetandroid.BD;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class RecetteWithCategorie {
    @Embedded
    public Recette recette;
    @Relation(
            parentColumn = "num_recette",
            entityColumn = "num_cat",
            associateBy = @Junction(CategorieRecetteCrossRef.class)
    )
    public List<Categorie> listCategorie;
}
