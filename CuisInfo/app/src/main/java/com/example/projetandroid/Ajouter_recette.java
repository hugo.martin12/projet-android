package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Ajouter_recette extends AppCompatActivity {

    public static final String EXTRA_TITRE =
            "package com.example.projetandroid.EXTRA_TITRE";
    public static final String EXTRA_DIFF =
            "package com.example.projetandroid.EXTRA_DIFF";
    public static final String EXTRA_URL =
            "package com.example.projetandroid.EXTRA_URL";
    public static final String EXTRA_INGR =
            "package com.example.projetandroid.EXTRA_INGR";
    public static final String EXTRA_DESC =
            "package com.example.projetandroid.EXTRA_DESC";

    private EditText et_ajouterR_titre;
    private EditText et_ajouterR_diff;
    private EditText et_ajouterR_url;
    private EditText et_ajouterR_ingr;
    private EditText et_ajouterR_desc;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_recette);

        et_ajouterR_titre = findViewById(R.id.et_editR_titre);
        et_ajouterR_diff = findViewById(R.id.et_editR_diff);
        et_ajouterR_url = findViewById(R.id.et_editR_url);
        et_ajouterR_ingr = findViewById(R.id.et_editR_ingr);
        et_ajouterR_desc = findViewById(R.id.et_editR_desc);
        button2 = findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ajouter_recette.this.saveRecette();
            }
        });
    }

    private void saveRecette() {
        String titre = et_ajouterR_titre.getText().toString();
        int diff = Integer.parseInt(et_ajouterR_diff.getText().toString());
        String url = et_ajouterR_url.getText().toString();
        String ingr = et_ajouterR_ingr.getText().toString();
        String desc = et_ajouterR_desc.getText().toString();

        if(titre.trim().isEmpty() || et_ajouterR_diff.getText().toString().trim().isEmpty() || url.trim().isEmpty() || ingr.trim().isEmpty() || desc.trim().isEmpty()){
            Toast.makeText(this,"Veuillez remplir les champs svp.",Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_TITRE,titre);
        intent.putExtra(EXTRA_DIFF,diff);
        intent.putExtra(EXTRA_URL,url);
        intent.putExtra(EXTRA_INGR,ingr);
        intent.putExtra(EXTRA_DESC,desc);

        setResult(RESULT_OK,intent);
        finish();

    }
}