package com.example.projetandroid;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.room.util.StringUtil;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Youtube extends YouTubeBaseActivity {

    private static String videoId;
    private String titreRecette ;
    private String api_key;
    private ThreadYoutube thread;
    private YouTubePlayer.OnInitializedListener OnInitializedListener;
    private YouTubePlayerView youTubePlayerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);

        Bundle bundle = getIntent().getExtras();
        this.titreRecette = bundle.getString("titreRecette");

        this.thread = new ThreadYoutube(this);
        this.api_key = "AIzaSyAuLRD_vWCS30d7aTSNYpqemK8C68rbj-s";
        this.youTubePlayerView= findViewById(R.id.youTubePlayerView);


        try {
            String result = new Connection().execute().get();
            Log.v("RESULT",result);
            this.initialiserYoutube(result);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public String getVideoId()  {
        URL url = null;

        try {
            String titreRecetteFormalize = StringUtils.stripAccents(this.getTitreRecette()).replaceAll("\\s+","+");
            url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&q=Recette+"+ titreRecetteFormalize+"&key="+this.getApi_key());
            Log.v("MES_LOGS", String.valueOf(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            con.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        try {
            int code = con.getResponseCode();
            if ( code == 200 ){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                StringBuilder id= new StringBuilder();
                while((inputLine = in.readLine()) != null){
                    if(inputLine.contains("videoId")){
                        String res = inputLine.substring(19);
                        res = res.replaceAll("\"","");
                        Log.v("MES_LOGS",res);
                        return res;
                    }
                }
                in.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        con.disconnect();
        return null;
    }

    public void initialiserYoutube(String idVideo){

        OnInitializedListener = new YouTubePlayer.OnInitializedListener(){

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                Log.v("MES_LOGS","ID video : "+idVideo);
                youTubePlayer.loadVideo(idVideo);
                youTubePlayer.play();
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(getApplicationContext(), "La vidéo youtube à échoué", Toast.LENGTH_SHORT).show();
                Log.v("MES_LOGS","ICICICI");
                Log.v("MES_LOGS",provider.toString());
                Log.v("MES_LOGS",youTubeInitializationResult.toString());
            }
        };

        this.youTubePlayerView.initialize(this.getApi_key(),OnInitializedListener);

    }

    public String getTitreRecette() {
        return titreRecette;
    }

    public String getApi_key() {
        return api_key;
    }



    public YouTubePlayerView getYouTubePlayerView() {
        return youTubePlayerView;
    }

    public static void setVideoId(String videoId) {
        Youtube.videoId = videoId;
    }

    public class Connection extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            String id = Youtube.this.getVideoId();

            return id;
        }
    }

}