package com.example.projetandroid.BD;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(primaryKeys = {"num_cat", "num_recette"})
public class CategorieRecetteCrossRef {
    @NonNull
    public Integer num_cat;
    @NonNull
    public Integer num_recette;

    public CategorieRecetteCrossRef(Integer num_cat, Integer num_recette) {
        this.num_cat = num_cat;
        this.num_recette = num_recette;
    }
}