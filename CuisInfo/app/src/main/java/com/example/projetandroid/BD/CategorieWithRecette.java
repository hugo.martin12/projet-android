package com.example.projetandroid.BD;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class CategorieWithRecette {
    @Embedded
    public Categorie cat;
    @Relation(
            parentColumn = "num_cat",
            entityColumn = "num_recette",
            associateBy = @Junction(CategorieRecetteCrossRef.class)
    )
    public List<Recette> listRecette;
}