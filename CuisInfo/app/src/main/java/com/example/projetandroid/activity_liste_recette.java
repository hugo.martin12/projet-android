package com.example.projetandroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.projetandroid.BD.Categorie;
import com.example.projetandroid.BD.CuisinfoViewModel;
import com.example.projetandroid.BD.Recette;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class activity_liste_recette extends AppCompatActivity {

    public static final int AJOUTER_RECETTE = 1;

    private RecyclerView recyclerView;
    private static RecyclerViewAdapterRecette mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static CuisinfoViewModel cuisinfoViewModel;
    private static String nom_cat;
    private Categorie cat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_recette);

        cuisinfoViewModel  = new ViewModelProvider(this).get(CuisinfoViewModel.class);
        Bundle bundle = getIntent().getExtras();
        nom_cat = bundle.getString("nom_cat");

        try {
            this.cat = cuisinfoViewModel.getCatNom(nom_cat);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.recyclerView =(RecyclerView) findViewById(R.id.rv_recette);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new RecyclerViewAdapterRecette(activity_liste_recette.this);
        recyclerView.setAdapter(mAdapter);

        activity_liste_recette.setListeRecette();

        //      cuisinfoViewModel.getAllRecette().observe(this, new Observer<List<Recette>>() {
  //          @Override
  //          public void onChanged(List<Recette> recettes) {
  //              mAdapter.setListeRecette(recettes);
  //          }
  //      });
    }


    public static void setListeRecette(){
        try {
            mAdapter.setListeRecette(cuisinfoViewModel.getRecetteWithCat(cuisinfoViewModel.getCatNom(nom_cat).getNum_cat()));
            mAdapter.notifyDataSetChanged();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void ajouterRecette(View view) {
        Intent intent = new Intent(this,Ajouter_recette.class);
        startActivityForResult(intent,AJOUTER_RECETTE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AJOUTER_RECETTE && resultCode == RESULT_OK){
            String titre = data.getStringExtra(Ajouter_recette.EXTRA_TITRE);
            int diff = data.getIntExtra(Ajouter_recette.EXTRA_DIFF,0);
            String url = data.getStringExtra(Ajouter_recette.EXTRA_URL);
            String ingr = data.getStringExtra(Ajouter_recette.EXTRA_INGR);
            String desc = data.getStringExtra(Ajouter_recette.EXTRA_DESC);
            Recette r = new Recette(titre,url,diff,ingr,desc);
            cuisinfoViewModel.insert_recette(r);


            try {
                cat = cuisinfoViewModel.getCatNom(nom_cat);

                Recette rBD = cuisinfoViewModel.getRecetteNom(r.getNom_recette());
                cuisinfoViewModel.insert_CAT_REC(cuisinfoViewModel.getRecetteID(rBD.getNum_recette()),cuisinfoViewModel.getCatID(cat.getNum_cat()));

                activity_liste_recette.setListeRecette();

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }



        }
    }

    public void supprimercategorie(View view) {
        cuisinfoViewModel.delete_cat(this.cat);
        finish();
    }
}