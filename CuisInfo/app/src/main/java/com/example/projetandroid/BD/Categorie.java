package com.example.projetandroid.BD;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "categorie")
public class Categorie {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer num_cat;
    @NonNull
    private String nom_cat;
    private String imageUrl;

    public Categorie(String nom_cat, String imageUrl) {
        this.nom_cat = nom_cat;
        this.imageUrl = imageUrl;
    }

    public String getNom_cat() {
        return nom_cat;
    }

    public void setNom_cat(String nom_cat) {
        this.nom_cat = nom_cat;
    }

    public Integer getNum_cat() {
        return num_cat;
    }

    public void setNum_cat(Integer num_cat) {
        this.num_cat = num_cat;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString(){
        return ""+this.num_cat+" / "+this.nom_cat+" / "+this.imageUrl;
    }
}
