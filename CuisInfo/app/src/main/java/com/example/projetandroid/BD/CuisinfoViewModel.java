package com.example.projetandroid.BD;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CuisinfoViewModel extends AndroidViewModel {
    private CuisinfoRepository repository;
    private LiveData<List<Recette>> allRecette;
    private LiveData<List<Categorie>> allCategorie;


    public CuisinfoViewModel(@NonNull Application application) throws ExecutionException, InterruptedException {
        super(application);
        repository = new CuisinfoRepository(application);
        allCategorie = repository.getALlCat();
        allRecette = repository.getALlRecette();
    }

    public void insert_recette(Recette recette){repository.insert_recette(recette);}
    public void insert_cat(Categorie cat){repository.insert_cat(cat);}
    public void insert_CAT_REC(Recette r,Categorie cat ){repository.insert_CAT_REC(r,cat);}



    public Recette getRecetteID(Integer id) throws ExecutionException, InterruptedException {return repository.getRecetteID(id);}
    public Categorie getCatID(Integer id) throws ExecutionException, InterruptedException {return repository.getCatID(id);}
    public Recette getRecetteNom(String nom) throws ExecutionException, InterruptedException {return repository.getRecetteNom(nom);}
    public Categorie getCatNom(String nom) throws ExecutionException, InterruptedException{return repository.getCatNom(nom);}

    public void update_recette(Recette recette){repository.update_recette(recette);}
    public void update_cat(Categorie categorie){repository.update_cat(categorie);}
    public void update_CAT_REC(Recette r,Categorie cat ){repository.update_CAT_REC(r,cat);}

    public void delete_CAT_REC(Recette r,Categorie cat ){repository.delete_CAT_REC(r,cat);}
    public void delete_recette(Recette recette){repository.delete_recette(recette);}
    public void delete_cat(Categorie cat){repository.delete_cat(cat);}

    public void delete_all_recette(){repository.delete_all_recette();}
    public void delete_all_cat(){repository.delete_all_cat();}
    public void delete_all_CAT_REC(){repository.delete_all_CAT_REC();}

    public List<CategorieWithRecette> getCatWithRecette(Integer id) throws ExecutionException, InterruptedException {return this.repository.getCatWithRecette(id);}
    public List<RecetteWithCategorie> getRecetteWithCat(Integer id) throws ExecutionException, InterruptedException {return this.repository.getRecetteWithCat(id);}


    public LiveData<List<Recette>> getAllRecette(){
        return this.allRecette;
    }

    public LiveData<List<Categorie>> getAllCategorie(){
        return this.allCategorie;
    }


}
