package com.example.projetandroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.projetandroid.BD.Categorie;
import com.example.projetandroid.BD.CuisinfoViewModel;
import com.example.projetandroid.BD.Recette;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public static final int AJOUTER_CAT = 1;
    private LiveData<List<Categorie>>listeCategorie;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private  CuisinfoViewModel cuisinfoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cuisinfoViewModel  = new ViewModelProvider(this).get(CuisinfoViewModel.class);

        this.recyclerView =(RecyclerView) findViewById(R.id.rv_liste_recette);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        final RecyclerViewAdapter mAdapter = new RecyclerViewAdapter(this);
        recyclerView.setAdapter(mAdapter);

        cuisinfoViewModel.getAllCategorie().observe(this, new Observer<List<Categorie>>() {
            @Override
            public void onChanged(List<Categorie> categories) {
                mAdapter.setCategorie(categories);
            }
        });


    }


    public void ajouterCategorie(View view) {
        Intent intent = new Intent(this,Ajouter_categorie.class);
        startActivityForResult(intent,AJOUTER_CAT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==  AJOUTER_CAT && resultCode == RESULT_OK) {
            String nom = data.getStringExtra(Ajouter_categorie.EXTRA_NOM);
            String url = data.getStringExtra(Ajouter_categorie.EXTRA_URL);

            Categorie cat = new Categorie(nom, url);

            cuisinfoViewModel.insert_cat(cat);

            Toast.makeText(this, "Catégorie crée !", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Aucune catégorie crée.", Toast.LENGTH_SHORT).show();
        }
    }

}